const moves = document.querySelector("[moves]");
const time = document.querySelector("[time]");
const score = document.querySelector("[score]");
const stopClick = document.getElementById("stop");
const cardsContainer = document.querySelector("[cards-container]");
const container = document.querySelector("[container]");
const result = document.getElementById("result");
const initialContainer = document.querySelector("[initial]");
const mediumStartClick = document.getElementById("medium-start");
const easyStartClick = document.getElementById("easy-start");
const hardStartClick = document.getElementById("hard-start");
const cardAudio = new Audio("./images/bubble-bursting-popping.mp3");
const winAudio = new Audio("./images/underwater-small-river.mp3");

let cards;
let interval;
let firstCard = false;
let secondCard = false;

const itemsArray = [{ name: "crab", imageLoc: "./images/crab.jpg" },
{ name: "golden-fish", imageLoc: "./images/golden-fish.jpg" },
{ name: "jelly-fish", imageLoc: "./images/jelly-fish.jpg" },
{ name: "octopus", imageLoc: "./images/octopus.jpg" },
{ name: "seahorse", imageLoc: "./images/seahorse.jpg" },
{ name: "star-fish", imageLoc: "./images/star-fish.jpg" },
{ name: "tortoise", imageLoc: "./images/tortoise.jpg" },
{ name: "whale", imageLoc: "./images/whale.jpg" },
{ name: "round-fish", imageLoc: "./images/round-fish.jpg"}];

let easyGame = false;
let mediumGame = true;
let hardGame = false;
let secondsCount = 0;
let minutesCount = 0;
let movesCount = 0;
let scoreCount = 0;
let size;

easyStartClick.addEventListener("click", ()=>{
    easyGame = true;
    mediumGame = false;
    hardGame = false;
    size = 4;
    cardsContainer.style.gridTemplateColumns = "repeat(4,1fr)";
    startGame();
});

mediumStartClick.addEventListener("click", () =>{
    easyGame = false;
    mediumGame = true;
    hardGame = false;
    size = 8;
    cardsContainer.style.gridTemplateColumns = "repeat(4,1fr)";
    startGame();
})

hardStartClick.addEventListener("click",()=>{
    easyGame = false;
    mediumGame = false;
    hardGame = true;
    size = 18;
    cardsContainer.style.gridTemplateColumns = "repeat(6,1fr)";
    startGame();
})

stopClick.addEventListener("click", (stopGame = () => {
    initialContainer.classList.remove("hide");
    stopClick.classList.add("hide");
    container.classList.add("hide");
    clearInterval(interval);
}));

function startGame() {
    secondsCount = 0;
    minutesCount = 0;
    movesCount = 0;
    scoreCount = 0;

    initialContainer.classList.add("hide");
    stopClick.classList.remove("hide");
    container.classList.remove("hide");

    interval = setInterval(timeCounter, 1000);
    moves.innerHTML = `<span>Moves: </span> ${movesCount}`;
    score.innerHTML = `<span>Cards Found: </span>${scoreCount}/${size}`;
    gameInitializer();
}

function gameInitializer() {
    result.innerText = "";
    scoreCount = 0;
    let cardsArray = shuffle(itemsArray);
    createDivsForCards(cardsArray);
}

function timeCounter() {
    secondsCount += 1;
    if (secondsCount >= 60) {
        minutesCount += 1;
        secondsCount = 0;
    }
    let seconds = secondsCount < 10 ? `0${secondsCount}` : secondsCount;
    let minutes = minutesCount < 10 ? `0${minutesCount}` : minutesCount;
    time.innerHTML = `<span>Time: </span>${minutes}:${seconds}`;
}

function movesCounter() {
    movesCount += 1;
    moves.innerHTML = `<span>Moves: </span>${movesCount}`;
}

function shuffle(array) {
    let counter = array.length;
    while (counter > 0) {
        let index = Math.floor(Math.random() * counter);
        counter--;
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }
    return array;
}

function createDivsForCards(cardsArray) {
    cardsContainer.innerHTML = "";
    let width;
    let height;
    if(easyGame)
    {
        cardsArray = [...cardsArray.slice(0,4), ...cardsArray.slice(0,4)];
        width = 4;
        height = 2;
    }
    else if(mediumGame)
    {
        cardsArray = [...cardsArray.slice(0,8), ...cardsArray.slice(0,8)];
        width = 4;
        height = 4;
    }
    else if(hardGame)
    {
        cardsArray = [...cardsArray, ...cardsArray, ...cardsArray, ...cardsArray];
        width = 6;
        height = 6;
    }
    
    cardsArray.sort(() => Math.random() - 0.5);
    for (let index = 0; index < width * height; index++) {
        cardsContainer.innerHTML += `<div class="card" card-value="${cardsArray[index].name}">
            <div class="card-up"><img src="./images/shell.jpg"></div>
            <div class="card-down"><img src="${cardsArray[index].imageLoc}"></div>
        </div>`;
    }
    handleCardClick();
}

function handleCardClick() {
    cards = document.querySelectorAll(".card");
    cards.forEach((card) => {
        card.addEventListener("click", () => {
            cardAudio.play();
            if (!card.classList.contains("matched") && !card.classList.contains("flipped")) {
                card.classList.add("flipped");
                if (!firstCard) {
                    firstCard = card;
                    firstCardValue = card.getAttribute("card-value");
                    //console.log("firstCard");
                }
                else {
                    secondCard = card;
                    let secondCardValue = card.getAttribute("card-value");
                    //console.log("Second card");

                    if (firstCardValue == secondCardValue) {
                        firstCard.classList.add("matched");
                        secondCard.classList.add("matched");

                        firstCard.style.boxShadow = "0 0 15px rgba(0, 255, 0, 0.8)";
                        secondCard.style.boxShadow = "0 0 15px rgba(0, 255, 0, 0.8)";

                        firstCard = false;
                        scoreCount += 1;
                        score.innerHTML = `<span>Cards Found: </span>${scoreCount}/${size}`;

                        //console.log("matched");

                        if (scoreCount == size) {
                            if(localStorage.getItem(`High Score ${size}`)===null || localStorage.getItem(`High Score ${size}`) < 100-movesCount)
                                localStorage.setItem(`High Score ${size}`, 100-movesCount);
                            setTimeout(() => {
                                winAudio.play();
                                result.innerHTML = `<img src="./images/win.gif">
                                            <h2>Collect the Pearl !</h2>
                                            <h4>Score: ${100-movesCount}%</h4>
                                            <h4>High Score: ${localStorage.getItem(`High Score ${size}`)}</h4>`;
                                stopGame();
                            }, 1500)
                        }
                    }
                    else {
                        //console.log("Not matched");
                        movesCounter();
                        let [tempFirstCard, tempSecondCard] = [firstCard, secondCard];
                        firstCard = false;
                        secondCard = false;
                        setTimeout(() => {
                            tempFirstCard.classList.remove("flipped");
                            tempSecondCard.classList.remove("flipped");
                        }, 1000);
                    }
                }
            }
        });
    });
}


